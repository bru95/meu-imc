package com.meuimc;

import org.junit.Test;

import static org.junit.Assert.*;

public class MainActivityTestNaoInstrumentado {
    @Test
    public void calculoIMCTest(){
        Float peso = 60.0f;
        Float altura = 1.80f;
        float actual = new MainActivity().calculoIMC(peso, altura);
        float expected = 18.52f;
        assertEquals("Calculo IMC falhou", expected, actual, 0.02);
    }
}
