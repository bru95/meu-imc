package com.meuimc;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private AlertDialog alerta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setaDados();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setaDados() {
        SharedPreferences sharedPref = this.getSharedPreferences("meuimcPreferences", this.MODE_PRIVATE);
        EditText pesoEdit = findViewById(R.id.peso);
        EditText alturaEdit = findViewById(R.id.altura);
        EditText idadeEdit = findViewById(R.id.idade);
        RadioButton feminino = findViewById(R.id.radio_feminino);
        RadioButton masculino = findViewById(R.id.radio_masculino);

        Float peso = sharedPref.getFloat("peso", -1);
        Float altura = sharedPref.getFloat("altura", -1);
        Integer idade = sharedPref.getInt("idade", -1);
        Float imc = sharedPref.getFloat("imc", -1);

        if (peso > -1) {
            pesoEdit.setText(String.valueOf(peso));
            alturaEdit.setText(String.valueOf(altura));
            idadeEdit.setText(String.valueOf(idade));
            feminino.setChecked(sharedPref.getBoolean("feminino", true));
            masculino.setChecked(sharedPref.getBoolean("masculino", false));

            mostraLinearLayout();
            TextView imc_label = findViewById(R.id.resultado_imc);
            imc_label.setText(String.format(getString(R.string.imc_result), imc));
            if (idade > 15) {
                mostraResultadoAdulto(imc);
            } else {
                mostraResultadoCrianca(imc, idade);
            }
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }
    }

    public void calculaIMC(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        if (checkPreenchimentoDados()) {

            EditText pesoEdit = findViewById(R.id.peso);
            EditText alturaEdit = findViewById(R.id.altura);
            EditText idadeEdit = findViewById(R.id.idade);
            RadioButton feminino = findViewById(R.id.radio_feminino);
            RadioButton masculino = findViewById(R.id.radio_masculino);

            Float peso = Float.parseFloat(pesoEdit.getText().toString());
            Float altura = Float.parseFloat(alturaEdit.getText().toString());
            Integer idade = Integer.parseInt(idadeEdit.getText().toString());

            Float imc = calculoIMC(peso, altura);

            SharedPreferences sharedPref = this.getSharedPreferences("meuimcPreferences", this.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putFloat("peso", peso);
            editor.putFloat("altura", altura);
            editor.putInt("idade", idade);
            editor.putFloat("imc", imc);
            editor.putBoolean("feminino", feminino.isChecked());
            editor.putBoolean("masculino", masculino.isChecked());
            editor.commit();

            mostraLinearLayout();
            TextView imc_label = findViewById(R.id.resultado_imc);
            imc_label.setText(String.format(getString(R.string.imc_result), imc));
            if (idade > 15) {
                mostraResultadoAdulto(imc);
            } else {
                mostraResultadoCrianca(imc, idade);
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Atenção");
            builder.setMessage("Por favor, preencha todos os campos.");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    alerta.dismiss();
                }
            });
            alerta = builder.create();
            alerta.show();
        }
    }

    public Float calculoIMC(Float peso, Float altura){
        return peso / (altura * altura);
    }

    private boolean checkPreenchimentoDados() {
        EditText pesoEdit = findViewById(R.id.peso);
        EditText alturaEdit = findViewById(R.id.altura);
        EditText idadeEdit = findViewById(R.id.idade);
        RadioButton feminino = findViewById(R.id.radio_feminino);
        RadioButton masculino = findViewById(R.id.radio_masculino);

        if (pesoEdit.getText().length() > 0 && alturaEdit.getText().length() > 0 &&
                idadeEdit.getText().length() > 0 &&
                (feminino.isChecked() || masculino.isChecked())) {
            return true;
        } else {
            return false;
        }
    }

    private void mostraResultadoAdulto(Float imc) {
        TextView resultado = findViewById(R.id.resultado);
        ImageView icon = findViewById(R.id.icon_result);

        if (imc < 17) {
            icon.setImageResource(R.mipmap.sad);
            resultado.setText("Resultado: MUITO ABAIXO DO PESO. Cuide-se!");
        } else if (imc >= 17 && imc <= 18.49) {
            icon.setImageResource(R.mipmap.neutro);
            resultado.setText("Resultado: ABAIXO DO PESO. Você está quase lá!");
        } else if (imc >= 18.50 && imc <= 24.99) {
            icon.setImageResource(R.mipmap.happy);
            resultado.setText("Parabéns seu IMC é NORMAL");
        } else if (imc >= 25 && imc <= 29.99) {
            icon.setImageResource(R.mipmap.neutro);
            resultado.setText("Resultado: ACIMA DO PESO. Você está quase lá!");
        } else if (imc >= 30 && imc <= 34.99) {
            icon.setImageResource(R.mipmap.neutro);
            resultado.setText("Resultado: OBESIDADE I. Cuide-se!");
        } else if (imc >= 35 && imc <= 39.99) {
            icon.setImageResource(R.mipmap.sad);
            resultado.setText("Resultado: OBESIDADE II (Severa). Cuide-se!");
        } else if (imc >= 40) {
            icon.setImageResource(R.mipmap.sad);
            resultado.setText("Resultado: OBESIDADE III (Mórbida). Cuide-se!");
        }
    }

    private void mostraResultadoCrianca(Float imc, Integer idade) {

        if (idade <= 6) {
            idade6(imc);
        } else if (idade == 7) {
            idade7(imc);
        } else if (idade == 8) {
            idade8(imc);
        } else if (idade == 9) {
            idade9(imc);
        } else if (idade == 10) {
            idade10(imc);
        } else if (idade == 11) {
            idade11(imc);
        } else if (idade == 12) {
            idade12(imc);
        } else if (idade == 13) {
            idade13(imc);
        } else if (idade == 14) {
            idade14(imc);
        } else if (idade == 15) {
            idade15(imc);
        }
    }

    private void idade6(Float imc) {
        RadioButton feminino = findViewById(R.id.radio_feminino);
        RadioButton masculino = findViewById(R.id.radio_masculino);
        TextView resultado = findViewById(R.id.resultado);
        ImageView icon = findViewById(R.id.icon_result);

        if (feminino.isChecked()) {
            if (imc <= 16.1) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 16.1 && imc <= 17.4) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        } else if (masculino.isChecked()) {
            if (imc <= 16.6) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 16.6 && imc <= 18) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        }
    }

    private void idade7(Float imc) {
        RadioButton feminino = findViewById(R.id.radio_feminino);
        RadioButton masculino = findViewById(R.id.radio_masculino);
        TextView resultado = findViewById(R.id.resultado);
        ImageView icon = findViewById(R.id.icon_result);

        if (feminino.isChecked()) {
            if (imc <= 17.1) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 17.1 && imc <= 18.9) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        } else if (masculino.isChecked()) {
            if (imc <= 17.3) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 17.3 && imc <= 19.1) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        }
    }

    private void idade8(Float imc) {
        RadioButton feminino = findViewById(R.id.radio_feminino);
        RadioButton masculino = findViewById(R.id.radio_masculino);
        TextView resultado = findViewById(R.id.resultado);
        ImageView icon = findViewById(R.id.icon_result);

        if (feminino.isChecked()) {
            if (imc <= 18.1) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 18.1 && imc <= 20.3) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        } else if (masculino.isChecked()) {
            if (imc <= 16.7) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 16.7 && imc <= 20.3) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        }
    }

    private void idade9(Float imc) {
        RadioButton feminino = findViewById(R.id.radio_feminino);
        RadioButton masculino = findViewById(R.id.radio_masculino);
        TextView resultado = findViewById(R.id.resultado);
        ImageView icon = findViewById(R.id.icon_result);

        if (feminino.isChecked()) {
            if (imc <= 19.1) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 19.1 && imc <= 21.7) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        } else if (masculino.isChecked()) {
            if (imc <= 18.8) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 18.8 && imc <= 21.4) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        }
    }

    private void idade10(Float imc) {
        RadioButton feminino = findViewById(R.id.radio_feminino);
        RadioButton masculino = findViewById(R.id.radio_masculino);
        TextView resultado = findViewById(R.id.resultado);
        ImageView icon = findViewById(R.id.icon_result);

        if (feminino.isChecked()) {
            if (imc <= 20.1) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 20.1 && imc <= 23.2) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        } else if (masculino.isChecked()) {
            if (imc <= 19.6) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 19.6 && imc <= 22.5) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        }
    }

    private void idade11(Float imc) {
        RadioButton feminino = findViewById(R.id.radio_feminino);
        RadioButton masculino = findViewById(R.id.radio_masculino);
        TextView resultado = findViewById(R.id.resultado);
        ImageView icon = findViewById(R.id.icon_result);

        if (feminino.isChecked()) {
            if (imc <= 21.1) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 21.1 && imc <= 24.5) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        } else if (masculino.isChecked()) {
            if (imc <= 20.3) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 20.3 && imc <= 23.7) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        }
    }

    private void idade12(Float imc) {
        RadioButton feminino = findViewById(R.id.radio_feminino);
        RadioButton masculino = findViewById(R.id.radio_masculino);
        TextView resultado = findViewById(R.id.resultado);
        ImageView icon = findViewById(R.id.icon_result);

        if (feminino.isChecked()) {
            if (imc <= 22.1) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 22.1 && imc <= 25.9) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        } else if (masculino.isChecked()) {
            if (imc <= 21.1) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 21.1 && imc <= 24.8) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        }
    }

    private void idade13(Float imc) {
        RadioButton feminino = findViewById(R.id.radio_feminino);
        RadioButton masculino = findViewById(R.id.radio_masculino);
        TextView resultado = findViewById(R.id.resultado);
        ImageView icon = findViewById(R.id.icon_result);

        if (feminino.isChecked()) {
            if (imc <= 23) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 23 && imc <= 27.7) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        } else if (masculino.isChecked()) {
            if (imc <= 21.9) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 21.9 && imc <= 25.9) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        }
    }

    private void idade14(Float imc) {
        RadioButton feminino = findViewById(R.id.radio_feminino);
        RadioButton masculino = findViewById(R.id.radio_masculino);
        TextView resultado = findViewById(R.id.resultado);
        ImageView icon = findViewById(R.id.icon_result);

        if (feminino.isChecked()) {
            if (imc <= 23.8) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 23.8 && imc <= 27.9) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        } else if (masculino.isChecked()) {
            if (imc <= 22.7) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 22.7 && imc <= 26.9) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        }
    }

    private void idade15(Float imc) {
        RadioButton feminino = findViewById(R.id.radio_feminino);
        RadioButton masculino = findViewById(R.id.radio_masculino);
        TextView resultado = findViewById(R.id.resultado);
        ImageView icon = findViewById(R.id.icon_result);

        if (feminino.isChecked()) {
            if (imc <= 24.2) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 24.2 && imc <= 28.8) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        } else if (masculino.isChecked()) {
            if (imc <= 23.6) {
                icon.setImageResource(R.mipmap.happy);
                resultado.setText("Parabéns seu IMC é NORMAL");
            } else if (imc > 23.6 && imc <= 27.7) {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: SOBREPESO. Cuide-se!");
            } else {
                icon.setImageResource(R.mipmap.neutro);
                resultado.setText("Resultado: OBESIDADE. Cuide-se!");
            }
        }
    }

    private void mostraLinearLayout() {
        LinearLayout layout = findViewById(R.id.linearLayoutResultado);
        layout.setVisibility(View.VISIBLE);
    }

    public void limpar(View view) {
        SharedPreferences sharedPref = this.getSharedPreferences("meuimcPreferences", this.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putFloat("peso", -1);
        editor.putFloat("altura", -1);
        editor.putInt("idade", -1);
        editor.putFloat("imc", -1);
        editor.putBoolean("feminino", true);
        editor.putBoolean("masculino", false);
        editor.commit();

        EditText pesoEdit = findViewById(R.id.peso);
        EditText alturaEdit = findViewById(R.id.altura);
        EditText idadeEdit = findViewById(R.id.idade);
        RadioButton feminino = findViewById(R.id.radio_feminino);
        RadioButton masculino = findViewById(R.id.radio_masculino);

        pesoEdit.setText("");
        alturaEdit.setText("");
        idadeEdit.setText("");
        feminino.setChecked(true);
        masculino.setChecked(false);

        LinearLayout layout = findViewById(R.id.linearLayoutResultado);
        layout.setVisibility(View.INVISIBLE);
    }
}
